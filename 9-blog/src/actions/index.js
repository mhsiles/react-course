import jsonPlaceholder from "../apis/jsonPlaceholder";
import _ from 'lodash';

//Action creator that calls both
export const fetchPostsAndUsers = () => async (dispatch, getState) => {
    // Get all the posts
    await dispatch(fetchPosts());
    // // Once loaded all the posts list, get all unique users and check their information
    // const userIds = _.uniq(_.map(getState().posts, 'userId'));
    // // Call the fetch function
    // userIds.forEach(id => dispatch(fetchUser(id)));

    //Alternative chain statement
    _.chain(getState().posts)
        .map('userId') //get user ids
        .uniq() //find uniq
        .forEach(id => dispatch(fetchUser(id))) //fetch each
        .value() //execute
};

export const fetchPosts = () => async (dispatch) => {

    const response = await jsonPlaceholder.get('/posts');
    
    dispatch({ type: 'FETCH_POSTS', payload: response.data });

};

export const fetchUser = (id) => async (dispatch) => {

    const response = await jsonPlaceholder.get(`/users/${id}`);
    
    dispatch({ type: 'FETCH_USER', payload: response.data });

};