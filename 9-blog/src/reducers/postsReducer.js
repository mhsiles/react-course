//Only return anything related to the information and action
export default (state = [], action) => {

    switch (action.type){
        case 'FETCH_POSTS':
            return action.payload;
        default:
            return state;
    }
    

}