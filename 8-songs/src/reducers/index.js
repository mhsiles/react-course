import { combineReducers } from "redux";

const songsReducer = () => {
    return [
        { title: 'I Want It That Way', duration: '4:00'},
        { title: 'Sweet Caroline', duration: '3:30'},
        { title: 'All Star', duration: '3:45'}
    ]
};

const selectedSongReducer = (selectedSong = null, action) => {
    if ( action.type === 'SONG_SELECTED' ){
        return action.payload;
    }

    return selectedSong;
};

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});