import React from 'react';

const CommentDetail = (props) => {
  return (
    <div className="comment">
        <a href="/" className="avatar">
          <img alt="avatar" src={'https://i.pravatar.cc/154?img=${id}'} />
        </a>
        <div className="content">
          <a href="/" className="author">
            {props.author}
          </a>
          <div className="metadata">
            <span className="date">
              {props.date}
            </span>
          </div>
          <div  className="text">
            {props.text}
          </div>
        </div>
      </div>
  );
}

export default CommentDetail;