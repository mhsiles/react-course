// Import React and ReactDOM
import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';
import faker from 'faker';

const App = () => {
  return (
    <div className="ui container comments">
      <ApprovalCard>
        <CommentDetail 
          author={faker.name.firstName()}
          date={faker.datatype.number()}
          text={faker.lorem.sentence()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail 
          author={faker.name.firstName()}
          date={faker.datatype.number()}
          text={faker.lorem.sentence()}
        />
      </ApprovalCard>
    </div>
  );
};


ReactDOM.render(
  <App />,
  document.querySelector('#root')
);

