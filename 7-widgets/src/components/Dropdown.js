import React, {useState, useEffect, useRef} from 'react';

const Dropdown = ({ options, selected, onSelectedChange, label }) => {

    const [open, setOpen] = useState(false);

    const ref = useRef();

    useEffect(() => {

        const onBodyClick = (event) => {
            // If clicked element is inside the ref (ui form), no closing necessary yet
            if (ref.current.contains(event.target)) return;
            // Else (element is outside ref), set false
            setOpen(false);
        };

        document.body.addEventListener('click', onBodyClick);

        // If the element is not on DOM, quit the listener
        return () => {
            document.body.removeEventListener('click', onBodyClick);
        };
    }, []);

    const renderedOptions = options.map((option,index) => {
        // Does not rerender the selected value
        if (option.value === selected.value) return null;

        return (
            <div
                onClick={() => onSelectedChange(option)} 
                key={index}
                className="item">
                    {option.label}
            </div>
        )
    })
    

    return (
        <div ref={ref} className="ui form">
            <div className="field">
                <label className="label">{label}</label>
                {/* Ternary classes  */}
                <div onClick={() => setOpen(!open)} className={`ui selection dropdown ${open ? 'visible active' : ''}`}>
                    <i className="dropdown icon"></i>
                    <div className="text">{ selected.label }</div>
                    <div className={`menu ${open ? 'visible transition' : ''}`}>
                        {renderedOptions}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Dropdown;