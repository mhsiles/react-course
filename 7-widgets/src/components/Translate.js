import React, {useState} from 'react';
import Dropdown from './Dropdown';
import Convert from './Convert';

const options = [
    {
        label: 'Español',
        value: 'es'
    },
    {
        label: 'Dutch',
        value: 'nl'
    },
    {
        label: 'Hindi',
        value: 'hi'
    }
]

const Translate = () => {

    const [language, setLanguage] = useState(options[1]);
    const [text, setText] = useState('Hola');

    return (
        <div>
            <div className="ui form">
                <div className="field">
                    <label>Enter Text:</label>
                    <input value={text} onChange={(e) => setText(e.target.value)}/>
                </div>
            </div>
            <Dropdown
                options={options}
                selected={language}
                onSelectedChange={setLanguage}
                label={"Select Language:"}
            />
            <hr />
            <h3>Output:</h3>
            <Convert language={language} text={text} />
        </div>

    )

};

export default Translate;