import React, {useState} from 'react';

const Accordion = ({items}) => {

    // first argument is the variable, second argument the setter function
    const [activeIndex, setActiveIndex] = useState(null);

    const onTitleClick = (index) => {
        setActiveIndex(index);
    }

    const itemList = items.map((item,index) => {

        // if index is currently active, add active class to element
        const active = index === activeIndex ? 'active' : ''

        return (
            // Generic JSX fragment
            <React.Fragment key={item.title}>
                <div className={`title ${active}`} onClick={() => onTitleClick(index)}>
                    <i className="dropdown icon"></i>
                    <p>{item.title}</p>
                </div>
                <div className={`content ${active}`}>
                    <p>{item.content}</p>
                </div>
            </React.Fragment>
        )
    });
    return (
        <div className="ui styled accordion">
            {itemList}
        </div>
    )
}

export default Accordion;