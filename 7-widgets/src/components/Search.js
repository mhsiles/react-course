import React, {useState, useEffect} from 'react';
import axios from 'axios';

const baseURL = 'https://en.wikipedia.org';

const Search = () => {

    const [term, setTerm] = useState('la rams');
    const [debouncedTerm, setDebouncedTerm] = useState(term);
    const [results, setResults] = useState([]);

    // Debounced term to avoid repeating queries
    useEffect(() => {

        const timeoutId = setTimeout(() => {
            setDebouncedTerm(term);
        }, 1000);

        return () => {
            clearTimeout(timeoutId);
        }

    }, [term]);
    
    // Only when term updates, search via useEffect is executed
    useEffect(() => {
        const search = async () => {
            const { data } = await axios.get(`${baseURL}/w/api.php`, {
                params: {
                    action: 'query',
                    list: 'search',
                    origin: '*',
                    format: 'json',
                    srsearch: term
                }
            });
            setResults(data.query.search);
        }

        search();

    }, [debouncedTerm]);

    // search results are mapped
    const renderedResults = results.map(result => {
        return (
            <div className="item" key={result.pageid}>
                <div className="right floated content">
                    <a className="ui button" href={`${baseURL}?curid=${result.pageid}`} target="_blank">
                        Go
                    </a>
                </div>
                <div className="content">
                    <div className="header">
                        {result.title}
                    </div>
                    {/* Potential XSS risk allowing rendering HTML. */}
                    <span dangerouslySetInnerHTML={{ __html: result.snippet }}></span>
                </div>
            </div>
        );
    })

    return (
        <div>
            <div className="ui form">
                <div className="field">
                    <label>Enter Search Term</label>
                    <input
                        className="input"
                        value={term}
                        onChange={e => setTerm(e.target.value)}
                    />
                </div>
            </div>
            <div className="ui celled list">
                { renderedResults }
            </div>
        </div>
    )
}

export default Search;