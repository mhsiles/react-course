import React, {useEffect, useState} from "react";
import axios from 'axios';

const { REACT_APP_GOOGLE_TRANSLATE_API_KEY } = process.env;

const Convert = ({language, text}) => {

    const [translated, setTranslated] = useState('');
    const [debouncedText, setDebouncedText] = useState('');

    useEffect(() => {

        const timeoutId = setTimeout(() => {
            setDebouncedText(text);
        }, 500);

        return (() => {
            clearTimeout(timeoutId);
        })

    }, [text])

    useEffect(() => {
    
        // Async function to access to the response data
        const doTranslation = async () => {
            const { data } = await axios.post('https://translation.googleapis.com/language/translate/v2', {}, {
                params: {
                    q: debouncedText,
                    target: language.value,
                    key: REACT_APP_GOOGLE_TRANSLATE_API_KEY
                }
            });
            setTranslated(data.data.translations[0].translatedText)
        };

        doTranslation();

    }, [language, debouncedText]);

    return (
        <div>
            <h1 className="ui header">
                {translated}
            </h1>
        </div>
    );

}

export default Convert;