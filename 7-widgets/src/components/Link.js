import React from 'react';

const Link = ({href, className, children}) => {

    const changeRoute = (e) => {

        if (e.metaKey || e.ctrlKey){
            return;
        }

        e.preventDefault();
        // update URL
        window.history.pushState({}, '', href);

        const navEvent = new PopStateEvent('popstate');
        window.dispatchEvent(navEvent);
    }

    return (
        <a  onClick={changeRoute} href={href} className={className}>
            {children}
        </a>
    )

};

export default Link;