# Getting started with React and Redux

---
### Description

This repository contians a series of small projects that cover overall the core React and Redux functionalities.

### Project Menu

+ tiktaktoe: project based on the React official documentation.
+ jsx: jsx syntax
+ components: component creation and props
+ seasons: class components
+ pictures: event handler and inputs

---
### Sources

[Tutorial: Intro to React](https://reactjs.org/tutorial/tutorial.html)
[Udemy Course Link](https://www.udemy.com/course/react-redux/)