import axios from 'axios';
const { REACT_APP_UNSPLASH_API_KEY } = process.env;

// Customize axios request
export default axios.create({
    baseURL: 'https://api.unsplash.com/',
    headers:{
        Authorization: 'Client-ID ' + REACT_APP_UNSPLASH_API_KEY
    },
})