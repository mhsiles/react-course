import React from 'react';
import unsplash from '../api/unsplash';
import SeachBar from './SearchBar';
import ImageList from './ImageList';

class App extends React.Component {

    state = { images: [] };

    // turn into arrow function to change 'this' context
    onSearchSubmit = (term) => {

        const endpoint = 'search/photos';
        // Request to obtain photos
        // Call directly to the unsplash service
        unsplash.get(endpoint,{
            params: { query: term }
        }).then(response => {
            this.setState({ images: response.data.results });
        });

    }

    render(){
        return (
            <div className="ui container" style={{ marginTop: '10px' }}>
                <SeachBar onSearchSubmit={ this.onSearchSubmit } />
                <ImageList images={this.state.images} />
            </div>
        );
    }
}

export default App;