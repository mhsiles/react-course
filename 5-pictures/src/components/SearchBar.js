import React from 'react';

class SearchBar extends React.Component {

    state = { term: '' };

    onFormSubmit = (event) => {
        // avoid the page to refresh
        event.preventDefault();

        // Call the parent function via props
        this.props.onSearchSubmit(this.state.term);
    }

    render() {
        return (
            <div className="ui segment">
                <form onSubmit={ this.onFormSubmit } className="ui form">
                    <div className="field">
                        <label>Image Search</label>
                        {/* <input type="text" onChange={this.onInputChange}/> */}
                        <input
                            type="text"
                            value={this.state.term}
                            onChange={(event) => this.setState( { term: event.target.value } )}    
                        />
                    </div>
                </form>
            </div>
        );
    }
}

export default SearchBar;